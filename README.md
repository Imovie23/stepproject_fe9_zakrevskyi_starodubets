<h1>Step project Forkio</h1>

<h2>Информация о проекте:</h2>

<b>Список использованных технологий:</b>

<i>
HTML, SASS, JavaScript, NodeJS, Gulp&Webpack, BEM
</i>


<b>Состав участников проекта:</b>
Misha Starodubets /  Roman Zakrevskyi 

<b>Задания выполнял</b> Roman Zakrevskyi:

<i>
<ul>
	<li>Сверстать шапку сайта с верхним меню (включая выпадающее меню при малом разрешении экрана).</li>
	<li>Сверстать секцию People Are Talking About Fork.</li>
	<li>Настройка Gulp&Webpack для проэкта.</li>
</ul>
</i>


<b>Задания выполнял</b> Misha Starodubets:

<i>
<ul>
	<li>Сверстать блок Revolutionary Editor.</li>
	<li>Светстать секцию Here is what you get.</li>
    <li>Сверстать секцию Fork Subscription Pricing.</li>
	<li>Разместить проект в интернете с помощью Github pages <a href="https://imovie23.github.io/Forkio/"><b>Forkio</a> </li>
</ul>
</i>